package com.innovate.paymob.businessobjects;

public class MobileAccount {
	
	private Long mobile;
	private double amountBalance;
	private String name;
	private long accountNumber;
	private long transactionHistory;
	
	public MobileAccount(Long mobile, double amountBalance, String name,
				  long accountNumber, long transactionHistory) {
		this.mobile = mobile;
		this.amountBalance = amountBalance;
		this.name = name;
		this.accountNumber = accountNumber;
		this.transactionHistory = transactionHistory;
	}
	public MobileAccount(double amountBalance, String name,
			  long accountNumber, long transactionHistory) {
	this.amountBalance = amountBalance;
	this.name = name;
	this.accountNumber = accountNumber;
	this.transactionHistory = transactionHistory;
}
	
	public Long getMobile() {
		return mobile;
	}
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}
	public double getAmountBalance() {
		return amountBalance;
	}
	public void setAmountBalance(double amountBalance) {
		this.amountBalance = amountBalance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public long getTransactionHistory() {
		return transactionHistory;
	}
	public void setTransactionHistory(long transactionHistory) {
		this.transactionHistory = transactionHistory;
	}
	
	

}
