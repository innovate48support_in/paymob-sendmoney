package com.innovate.paymob.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innovate.paymob.businessobjects.MobileAccount;
import com.innovate.paymob.businessobjects.TransferResponse;
import com.innovate.paymob.constants.CommonConstants;
import com.innovate.paymob.utils.MoneyUtility;

@Service
public class MoneySendService {
	
	@Autowired
	MoneyUtility utility;
	
	@Autowired
	TransactionHistoryUpdater transactionHistoryUpdater;
	
	Map<Long, MobileAccount> accounts = null;
	
	{
		if(accounts==null || accounts.size()==0) {
			accounts = new HashMap<>();
			for(int i=0; i<CommonConstants.mobileNumberList.size();i++) {
				CommonConstants.accountList.get(i).setMobile(CommonConstants.mobileNumberList.get(i));
				accounts.put(CommonConstants.mobileNumberList.get(i), CommonConstants.accountList.get(i));
			}
		}
	}
	
	public TransferResponse transferMoney(Long sender, Long receiver, double amount) {
		
		TransferResponse response = utility.transferMoney(sender, receiver, amount, accounts);
		
		boolean isPaymentHistoryUpdatedForSender = transactionHistoryUpdater.updatePaymentHistory(sender ,amount);
		if(isPaymentHistoryUpdatedForSender) {
			System.out.println("Payemnt History updates for Mobile:" + sender + " is successfull.");
		}else {
			System.out.println("Payemnt History updates for Mobile:" + sender + " is failure!!!");
		}
		
		boolean isPaymentHistoryUpdatedForReceiver = transactionHistoryUpdater.updatePaymentHistory(receiver ,amount);
		if(isPaymentHistoryUpdatedForReceiver) {
			System.out.println("Payemnt History updates for Mobile:" + receiver + " is successfull.");
		}else {
			System.out.println("Payemnt History updates for Mobile:" + receiver + " is failure!!!");
		}
		return response;
	}

	
	
}
