package com.innovate.paymob.utils;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.innovate.paymob.businessobjects.MobileAccount;
import com.innovate.paymob.businessobjects.TransferResponse;

@Component
public class MoneyUtility {

	public TransferResponse transferMoney(Long sender, Long receiver, double amount
										  , Map<Long, MobileAccount> accounts) {
		MobileAccount senderAcct = accounts.get(sender);
		MobileAccount receiverAcct = accounts.get(receiver);
		TransferResponse response = new TransferResponse();
		if(senderAcct==null ||  receiverAcct==null) {
			response.setStatus("FAILURE");
			response.setMessage("Money Transfer is unsuccessful, since Sender or Receiver Mobile Account not found!!!");
		}else {
			senderAcct.setAmountBalance(senderAcct.getAmountBalance()-amount);
			senderAcct.setTransactionHistory(senderAcct.getTransactionHistory()+(long)amount);
			
			receiverAcct.setAmountBalance(receiverAcct.getAmountBalance()+amount);
			receiverAcct.setTransactionHistory(receiverAcct.getTransactionHistory()+(long)amount);
			
			response.setStatus("SUCCESS");
			response.setMessage("Money Transfer is successful, Current Balance is: " + senderAcct.getAmountBalance() + " INR");
		}
		
		return response;
	}

	
}
